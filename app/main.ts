import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TaskModule } from './task/task.module';

platformBrowserDynamic().bootstrapModule(TaskModule);
