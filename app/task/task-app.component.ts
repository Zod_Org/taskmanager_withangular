/**
 * Created by themaster on 05.01.17.
 */
import { Component }          from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'task-app',
	template: `
    <h1>{{ title }}</h1>
    <nav>
      <a routerLink="/tasks" routerLinkActive="active">Tasks</a>
    </nav>
    <router-outlet></router-outlet>
  `
})
export class TaskAppComponent {
	title = 'Tasks';
}
