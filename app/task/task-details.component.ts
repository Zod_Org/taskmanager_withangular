import { Component, Input } from '@angular/core';
import { Task } from './task';

@Component({
	selector: 'task-details',
	styleUrls: ['app/task/task.css'],
	template: `

		<div *ngIf="task" id="{{ task.number }}" #task_id class="details">
			<div class="ui three column grid">
				<div class="row">
				  <div class="column">{{ task.customer }}</div>
				  <div class="column">{{ task.contract }}</div>
				  <div class="column">{{ task.sum }}</div>
				</div>
			</div>
			<div>
				 <p>{{ task.title }} №{{task.number}} від {{task.date}}</p>
				 <p>{{ task.description }}</p>
				 <div class="ui buttons todo-btn-set">
					 <button class="ui button todo-btn-reject" (click)="rejectTask($event, task_id)">Відхилити</button>
					 <div class="or"></div>
					 <button class="ui positive button todo-btn-confirm" (click)="confirmTask($event, task_id)">Погодити</button>
				 </div>
			</div>		
		</div>
`,
})
export class TaskDetailsComponent {

	@Input()

	task: Task[];

	rejectTask(event: any, task_id: any) {
		console.log("Task " + task_id.id + " is rejected!");
		return false;
	}
	confirmTask(event: any, task_id: any) {
		console.log("Task " + task_id.id + " is confirmed!");
		return false;
	}
}