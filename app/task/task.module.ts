import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { TaskAppComponent } from './task-app.component';
import { TasksComponent } from './tasks.component';
import { TaskDetailsComponent } from "./task-details.component";
import { TaskRoutingModule } from './task-routing.module';
import { HttpModule } from '@angular/http';

@NgModule({
  imports:      [
                    BrowserModule,
                    HttpModule,
                    TaskRoutingModule
                ],
  declarations: [
                    TaskAppComponent,
                    TasksComponent,
                    TaskDetailsComponent
                ],

  bootstrap:    [ TaskAppComponent ]
})
export class TaskModule {}
