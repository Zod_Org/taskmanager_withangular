import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';
import { Task } from './task';
import { TaskService } from './task.service';

@Component({
  selector: 'tasklist',
  providers: [TaskService],
  styleUrls: ['app/task/task.css'],
  template: `

<table class="ui striped unstackable table">
  <thead>
    <tr>
    	<th>Контрагент</th>
    	<th>Договір</th>
    	<th>Сума</th>
  	</tr>
  </thead>
  <tbody>
    <tr
	    *ngFor="let task of tasks;"
		id="{{ task.number }}" #task_id
		(click)="onSelect(task)"
		[class.selected]="task === selectedTask"
	>
      <td>{{ task.customer }}</td>
      <td>{{ task.contract }}</td>
      <td class="right aligned">{{ task.sum }}</td>
    </tr>
  </tbody>
</table>

<task-details [task]="selectedTask"></task-details>
`,
})
export class TasksComponent implements OnInit {
	constructor(
		private _taskService: TaskService,
		private router: Router
	) {}

	tasks: Task[];
	selectedTask: Task;

	onSelect(task: Task): void {
		this.selectedTask = task;
		this.router.navigate(['/detail', this.selectedTask.id]);
	}

	ngOnInit() {
		this.getTasks();
	}

	getTasks() {
		this._taskService.getTasks().then(tasks => this.tasks = tasks);
	}
}