/**
 * Created by themaster on 27.12.16.
 */
import {Injectable} from "@angular/core";
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Task } from './task';

@Injectable ()

export class TaskService {

	private heroesUrl = 'http://192.168.5.168/restws/hs/tasks/list';  // URL to web api

	// this.defaultHeaders.set('Authorization', 'Basic ' + btoa(this.username + ':' + this.password));

	private headers = new Headers({
		'Content-Type': 'text/plain',
		// 'Authorization': 'Basic c3J2YzoxMjM0NTY=',
	});

	constructor(private http: Http) {}

	getTasks(): Promise<Task[]> {
		return this.http.get(this.heroesUrl,
				{
					headers: this.headers,
					// withCredentials: true
				}
			)
			.toPromise()
			.then(response => response.json() as Task[])
			.catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error); // for demo purposes only
		return Promise.reject(error.message || error);
	}

}
