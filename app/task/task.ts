/**
 * Created by themaster on 27.12.16.
 */

export class Task {
	id : string;
	customer : string;
	contract : string;
	sum : string;
	number : string;
	date : string;
	title : string;
	description : string;
}
