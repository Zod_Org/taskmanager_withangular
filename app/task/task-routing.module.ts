/**
 * Created by themaster on 05.01.17.
 */
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TasksComponent }      from './tasks.component';
import { TaskDetailsComponent }  from './task-details.component';

const routes: Routes = [
	{ path: '', redirectTo: '/tasks', pathMatch: 'full' },
	{ path: 'tasks',  component: TasksComponent },
	{ path: 'detail/:id', component: TaskDetailsComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class TaskRoutingModule {}
